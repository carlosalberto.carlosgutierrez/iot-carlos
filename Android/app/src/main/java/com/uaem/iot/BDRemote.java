package com.uaem.iot;

import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class BDRemote {
  private MainActivity mainActivity;
  private DatabaseReference referenceNodeFirebase;
	public BDRemote(MainActivity mainActivity){
    this.mainActivity = mainActivity;
  	//Se inicializa una instancia
		FirebaseApp.initializeApp(mainActivity);
		// Obtén una referencia a la base de datos
		// Obtén una referencia a un nodo específico
		this.referenceNodeFirebase = FirebaseDatabase.getInstance().getReference("door");
		// Leer datos desde la base de datos
		this.referenceNodeFirebase.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        // Manejar los datos leídos aquí
        //Log.d(MainActivity.TAG, "Valor Init: " + mainActivity.doors.toString());
        boolean isInit = mainActivity.doors.name == null;
        mainActivity.doors = dataSnapshot.getValue(Doors.class);
        //Log.d(MainActivity.TAG, "Valor leído: " + mainActivity.doors.toString());
        if(isInit)mainActivity.doors.isOpen = false;
        if (mainActivity.doors.isOpen) mainActivity.arduinoConnect.openDoor();
      }
      @Override
      public void onCancelled(DatabaseError error) {
          Log.w(MainActivity.TAG, "Error al leer datos.", error.toException());
      }
		});
	}
	public void setFirebase(){
		// Escribir datos en la base de datos
		this.referenceNodeFirebase.setValue(this.mainActivity.doors);
	}
}