package com.uaem.iot;
import java.util.List;
public class Doors {
  public String name;
  public boolean isOpen;
  public String photo;
  public List<String> permitidos;
  public Doors(){}

  @Override
  public String toString() {
    return "Doors{" +
            "name='" + name + '\'' +
            ", isOpen=" + isOpen +
            ", photo='" + photo + '\'' +
            ", permitidos=" + permitidos +
            '}';
  }
}