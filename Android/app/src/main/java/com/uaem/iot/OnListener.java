package com.uaem.iot;
public interface OnListener<TResult> {
  void on(TResult result);
}