package com.uaem.iot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
  public static final String TAG = "Iot-UAEM";
	public Doors doors;//Objeto que transmite a la BD remota de Firebase
  public BDRemote bdRemote;//la base de datos remota que se encuentra en firebase
  private TakePhoto takePhoto;
	private EditText editTextName;
  private TextView txtAvisos, txtAvisosUsb;
  //control del Arduino por el puerto serial USB
	public ArduinoConnect arduinoConnect;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.doors = new Doors();
    bdRemote = new BDRemote(this);
    arduinoConnect = new ArduinoConnect(this);
    takePhoto = new TakePhoto(this);
		this.editTextName = findViewById(R.id.editTextName);
    this.txtAvisos = findViewById(R.id.textViewAvisos);
    this.txtAvisosUsb = findViewById(R.id.textViewAvisosUSB);
    findViewById(R.id.buttonRequestAccess).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clickOpenDoor();
      }
		});
    intentFilterUsb();
  }
  private void intentFilterUsb(){
    IntentFilter filter = new IntentFilter();
    filter.addAction(arduinoConnect.ACTION_USB_PERMISSION);
    filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
    filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
    registerReceiver(broadcastReceiverUsb, filter);
  }
  private final BroadcastReceiver broadcastReceiverUsb = new BroadcastReceiver() { // Broadcast Receiver to automatically
    // start and stop the serial connection.
    @Override
    public void onReceive(Context context, Intent intent) {
      arduinoConnect.onReceive(intent);
    };
  };
  private void clickOpenDoor(){
    this.setAvisos("Socilitando acceso a CENTRAL", Color.GREEN);
    doors.name = editTextName.getText().toString();
    editTextName.setText("");
    if(doors.name.equals("")){
      this.setAvisos("Ingrese NOMBRE", Color.BLACK);
      return;
    }
    if(doors.permitidos != null) {
      if (doors.permitidos.contains(doors.name)) {
        //si ya esta registrado el nombre, simplemente abrimos la puerta
        arduinoConnect.openDoor();
        return;
      }
    }
    takePhoto.obtainPhoto(new OnListener<String>(){
      @Override
      public void on(String resultado) {
        // Agregar la imagen Base64 al objeto Door.photo
        doors.photo = resultado;
        bdRemote.setFirebase();
      }
    });
  }
  public void setAvisos(String aviso, int color){
    this.txtAvisos.setText(aviso);
    this.txtAvisos.setTextColor(color);
  }
  public void setAvisosUsb(String aviso, int color){
    this.txtAvisosUsb.setText(aviso);
    this.txtAvisosUsb.setTextColor(color);
  }
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    takePhoto.onActivityResult(requestCode, resultCode, data);
	}	
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    this.takePhoto.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
  @Override
  protected void onResume() {
    super.onResume();
    intentFilterUsb();
  }
  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(broadcastReceiverUsb);
  }
  @Override
  protected void onDestroy() {
    super.onDestroy();
    unregisterReceiver(broadcastReceiverUsb);
    arduinoConnect.onDestroy();
  }
}