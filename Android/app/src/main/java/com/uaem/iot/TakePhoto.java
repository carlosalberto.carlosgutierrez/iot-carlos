package com.uaem.iot;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TakePhoto {
  //para tomar una foto y enviarla al firebase
  private String currentPhotoPath;
  private MainActivity mainActivity;
  private static final int REQUEST_IMAGE_CAPTURE = 1;
  private OnListener<String> listener;
  private static final int REQUEST_CAMERA_PERMISSION = 101;
  private static final int REQUEST_STORAGE_PERMISSION = 102;
  public TakePhoto(MainActivity mainActivity) {
    this.mainActivity = mainActivity;
    // Solicitar permisos de cámara (asegúrate de haber declarado los permisos en el manifiesto)
    if (ActivityCompat.checkSelfPermission(mainActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(mainActivity, new String[]{android.Manifest.permission.CAMERA}, 1);
    }
  }
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    if (requestCode == REQUEST_CAMERA_PERMISSION) {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permiso de la cámara concedido, verifica y solicita permiso de escritura en el almacenamiento
        if (ContextCompat.checkSelfPermission(this.mainActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
          ActivityCompat.requestPermissions(this.mainActivity,
                  new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                  REQUEST_STORAGE_PERMISSION);
        } else {
          // Permiso de escritura en el almacenamiento ya concedido, puedes guardar la imagen
          // Realiza la captura y el guardado de la imagen aquí
          this.takePhotoWithCamera();
        }
      } else {
        this.mainActivity.setAvisos("Permite el permiso de la CAMARA...", Color.RED);
      }
    } else if (requestCode == REQUEST_STORAGE_PERMISSION) {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permiso de escritura en el almacenamiento concedido, puedes guardar la imagen
        // Realiza la captura y el guardado de la imagen aquí
        this.takePhotoWithCamera();
      } else {
        this.mainActivity.setAvisos("Permite el permiso de ESCRITURA...", Color.RED);
      }
    }
  }
  public void obtainPhoto(OnListener<String> listener) {
    this.listener = listener;
    // Verificar y solicitar permiso de la cámara
    if (ContextCompat.checkSelfPermission(this.mainActivity, android.Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this.mainActivity,
              new String[]{android.Manifest.permission.CAMERA},
              REQUEST_CAMERA_PERMISSION);
    } else {
      // Permiso de la cámara ya concedido, puedes capturar la imagen
      // Luego, verifica y solicita permiso de escritura en el almacenamiento
      if (ContextCompat.checkSelfPermission(this.mainActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
              != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(this.mainActivity,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_STORAGE_PERMISSION);
      } else {
        // Permiso de escritura en el almacenamiento ya concedido, puedes guardar la imagen
        // Realiza la captura y el guardado de la imagen aquí
        this.takePhotoWithCamera();
      }
    }
  }
  private void takePhotoWithCamera() {
    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    if (takePictureIntent.resolveActivity(mainActivity.getPackageManager()) != null) {
      File photoFile = null;
      try {
        photoFile = createImageFile();
      } catch (IOException ex) {
        // Manejar la excepción en caso de error al crear el archivo
      }
      if (photoFile != null) {
        Uri photoURI = FileProvider.getUriForFile(this.mainActivity,
                "com.uaem.iot.fileprovider",
                photoFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        mainActivity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
      }
    }
  }
  private File createImageFile() throws IOException {
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageFileName = "JPEG_" + timeStamp + "_";
    File storageDir = mainActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    File image = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
    );
    currentPhotoPath = image.getAbsolutePath();
    return image;
  }
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
      Bitmap imageBitmap = BitmapFactory.decodeFile(currentPhotoPath);
      // Convertir la imagen a Base64
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      imageBitmap.compress(Bitmap.CompressFormat.JPEG, 25, baos);
      byte[] byteArray = baos.toByteArray();
      String base64Image = Base64.encodeToString(byteArray, Base64.DEFAULT);
      if (this.listener != null) this.listener.on(base64Image);
    }
  }
}