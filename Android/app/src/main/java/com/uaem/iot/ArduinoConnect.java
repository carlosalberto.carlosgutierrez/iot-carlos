package com.uaem.iot;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.CountDownTimer;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ArduinoConnect{
  private MainActivity mainActivity;
  public final String ACTION_USB_PERMISSION = "com.uaem.iot.USB_PERMISSION";
  UsbDevice device;
  UsbManager usbManager;
  UsbSerialDevice serialPort;
  UsbDeviceConnection connection;
  public ArduinoConnect(MainActivity mainActivity) {
    this.mainActivity = mainActivity;
    usbManager = (UsbManager)mainActivity.getSystemService(Context.USB_SERVICE);
  }
  public void onReceive(Intent intent){
    String action = intent.getAction();
    if(action == null)return;
    if (action.equals(ACTION_USB_PERMISSION)){
      boolean granted = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, true);
      mainActivity.setAvisosUsb("GRANTED is "+granted, Color.YELLOW);
      if (granted){
        connection = usbManager.openDevice(device);
        serialPort = UsbSerialDevice.createUsbSerialDevice(device,connection);
        if (serialPort != null){
          if (serialPort.open()){ // Set serial conection parameters
            serialPort.setBaudRate(9600);
            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
            serialPort.read(mCallBack);
            mainActivity.setAvisosUsb("Puerta conectada!!!", Color.GREEN);
          }else {
            mainActivity.setAvisosUsb("Puerta NO conectada", Color.RED);
          }
        }else {
          mainActivity.setAvisosUsb("PORT IS NULL", Color.RED);
        }
      }else{
        mainActivity.setAvisosUsb("Falta el permiso de USB", Color.RED);
      }
    }else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)){
      mainActivity.setAvisosUsb("USB CONECTADO", Color.GREEN);
      initConnect();
    }else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)){
      mainActivity.setAvisosUsb("USB DESCONECTADO", Color.RED);
      onDestroy();
    }
  }
  UsbSerialInterface.UsbReadCallback mCallBack = new UsbSerialInterface.UsbReadCallback(){
    // DEfining CallBack which triggers whenever data is read.
    @Override
    public void onReceivedData(byte[] arg0){
      String data = null;
      try {
        data = new String(arg0, "UTF-8");
        data.concat("\n");
      }catch (UnsupportedEncodingException e){
        e.printStackTrace();
      }
    }
  };
  public void initConnect() {
    HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
    if (!usbDevices.isEmpty()){
      boolean keep = true;
      for (Map.Entry<String, UsbDevice>entry:usbDevices.entrySet()){
        device = entry.getValue();
        if(device.toString().contains("Arduino")){
          PendingIntent pi = PendingIntent.getBroadcast(this.mainActivity,
                  0,
                  new Intent(ACTION_USB_PERMISSION),
                  PendingIntent.FLAG_IMMUTABLE);
          usbManager.requestPermission(device, pi);
          keep = false;
        }else{
          connection = null;
          device = null;
        }
        if (!keep)break;
      }
    }
  }
  public void onDestroy() {
    if(serialPort != null)serialPort.close();
  }
  public void openDoor(){
    this.sendCommand("O");
    new CountDownTimer(4000, 1000) {
      public void onTick(long millisUntilFinished) {
        // Se llama en cada tick (cada segundo en este caso)
        //long segundosRestantes = millisUntilFinished / 1000;
      }
      public void onFinish() {
        closeDoor();
      }
    }.start(); // Iniciar el temporizador
  }
  public void closeDoor(){
    this.sendCommand("C");
    this.mainActivity.doors.isOpen = false;
    this.mainActivity.bdRemote.setFirebase();
  }
  //'O' para abrir, '?' para cerrar
  public void sendCommand(String string) {
    this.mainActivity.setAvisos(string.equals("O")?"ABIERTO":"CERRADO", string.equals("O")?Color.GREEN:Color.RED);
    if(serialPort != null)
      serialPort.write(string.getBytes());
    else
      this.mainActivity.setAvisosUsb("USB serialPort is NULL", Color.RED);
  }
}