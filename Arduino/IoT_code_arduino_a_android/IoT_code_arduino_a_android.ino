/*
Código conexion ArduinoUNO
Este programa escucha comandos a través del puerto serial y 
controla el pin 13 en función del comando recibido
'O' -> Open, Abrir, encender
'?' -> cualquier otro caracter -> Close, Cerrar, apagar
*/
const int led = 13;
void setup(){
    pinMode(led, OUTPUT);
    Serial.begin(9600);
}
void loop(){
	if (Serial.available())
    digitalWrite(led, Serial.read() == 'O');
}
