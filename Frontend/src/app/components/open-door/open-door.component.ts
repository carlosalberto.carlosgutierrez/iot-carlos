import { CommonModule } from '@angular/common';
import { Component, Injectable, OnInit } from '@angular/core';
import { BDService } from 'src/app/services/bd.service';
import { isEmpty } from 'src/app/services/utils.service';

@Component({
  selector: 'app-open-door',
  templateUrl: './open-door.component.html',
  styleUrls: ['./open-door.component.scss'],
  standalone: true,
  imports: [CommonModule]
})
export class OpenDoorComponent implements OnInit {
  public DOORS: Doors = {
    name: "en espera...",
    isOpen: false,
    photo: "./favicon.ico",
    permitidos: ["zapata", "amores", "reyes"]
  };
  constructor(private bd: BDService) { }
  ngOnInit(): void {
    this.bd.on(KEY_DOOR, (door: Doors) => {
      this.DOORS = door;
      if (isEmpty(this.DOORS.name)) this.DOORS.name = "En espera...";
      if (isEmpty(this.DOORS.photo)) this.DOORS.photo = "./favicon.ico";
      if (!this.DOORS.photo.startsWith("data:image/png;base64,") && this.DOORS.photo != "./favicon.ico")
        this.DOORS.photo = "data:image/png;base64," + this.DOORS.photo;
    });
  }
  public resetData(): void {
    this.bd.set(KEY_DOOR, {
      name: "",
      isOpen: false,
      photo: "",
      permitidos: ["zapata", "amores", "reyes"]
    });
  }
  public openDoor(): void {
    this.DOORS.name = "";
    this.DOORS.photo = "";
    this.DOORS.isOpen = true;
    this.bd.set(KEY_DOOR, this.DOORS);
    setTimeout(() => {
      this.DOORS.isOpen = false;
      this.bd.set(KEY_DOOR, this.DOORS);
    }, 4000);
  }
}
interface Doors {
  name: string,
  isOpen: boolean,
  photo: string,
  permitidos: string[]
}
const KEY_DOOR = "door";