import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss'],
  standalone: true,
  imports: [
    CommonModule
  ]
})
export class SplashComponent {
  constructor(private router: Router) {
    this.isReady();
  }
  private async isReady(): Promise<void> {
    const time = setTimeout(() => {
      clearTimeout(time);
      this.router.navigate(["home"]);
    }, 2000);
  }
}