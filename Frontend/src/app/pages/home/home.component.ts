import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { OpenDoorComponent } from 'src/app/components/open-door/open-door.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    OpenDoorComponent
  ]
})
export class HomeComponent {}