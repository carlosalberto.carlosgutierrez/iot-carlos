import { initializeApp } from "firebase/app";
import { getDatabase, ref, set, onValue, Database } from "firebase/database";
import { Injectable } from "@angular/core";
import { isEmpty, isFull } from "./utils.service";
/**
 * @author Carlos Alberto Gutierrez Zapata
 * @abstract Clase base de datos para recalizar CRUD tanto online en Firebase
 */
@Injectable({ providedIn: 'root' })
export class BDService {
  constructor(private firebase: FirebaseService) { }
  private noIsValidKey(k: string): boolean {
    if (isEmpty(k)) return true;//error controlado
    k = k.replaceAll(" ", "/");
    if (k.includes("//")) {
      console.error("existe un nulo en la key\n", k);
      return true;
    }
    let nodos = k.split("/");
    for (let nodo of nodos) {
      if (isEmpty(nodo)) {
        console.error("existe un nulo en la key\n", k);
        return true;
      }
    }
    return false;
  }
  public set(k: string, v: any): void {
    if (this.noIsValidKey(k)) return;
    if (isEmpty(v)) v = "";
    this.firebase.set(k, v);
  }
  public on(k: string, c: Function): void {
    if (this.noIsValidKey(k)) return;
    //console.log("Solicitando a REMOTO firebase con key["+k+"]");
    this.firebase.on(k, (data: any, key: string) => {
      if (isFull(data)) {
        //console.log("k -> ", k + "/" + key, "\nv -> ", data);
        c(data, key);
      }
    });
  }
}
@Injectable({ providedIn: 'root' })
class FirebaseService {
  private node: Database = getDatabase(initializeApp({ databaseURL: "https://iot-uaem-default-rtdb.firebaseio.com/", }));
  private get(k: string, c: Function, only: boolean) {
    onValue(ref(this.node, k), (snapshot) => {
      c(snapshot.val(), k);
    }, { onlyOnce: only });
  }
  public set(k: string, v: any): void {
    if (isEmpty(v)) v = "";
    set(ref(this.node, k), v);
  }
  public on(k: string, c: Function): void {
    const listen = (data: any, key: string) => {
      if (isEmpty(key)) key = "";
      if (isEmpty(data)) return;
      c(data, key);
    };
    this.get(k, listen, false);
  }
}