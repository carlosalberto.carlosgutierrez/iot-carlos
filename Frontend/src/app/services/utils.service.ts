import { Injectable } from "@angular/core";
/**
 * @author Carlos Alberto Gutierrez Zapata
 * @abstract Conjunto de metodos constantes para ser utilizados de manera anonima desde cualquier clase sin tener que inyectar
*/
@Injectable({ providedIn: 'root' })
export class UtilsService { }
export const RANDOM_get = (min: number, max: number) => {
  return Math.round(Math.random() * (max - min) + min);
};
export const DIGIT_get = (digitos: number) => {
  if (digitos < 0) digitos = digitos * (-1);
  while (digitos > 9)
    digitos = digitos.toString().split('').reduce(function (r, n) { return r + parseInt(n) }, 0);
  return digitos;
};
export const SCREEN_isBottom = () => {
  return (window.scrollY + window.innerHeight) >= document.body.scrollHeight;
};
export const DATE_getTimeAll = () => {
  const fecha = new Date();
  const anio = fecha.getFullYear().toString();
  const mes = (fecha.getMonth() + 1).toString().padStart(2, '0');
  const dia = fecha.getDate().toString().padStart(2, '0');
  const hora = fecha.getHours().toString().padStart(2, '0');
  const minutos = fecha.getMinutes().toString().padStart(2, '0');
  const segundos = fecha.getSeconds().toString().padStart(2, '0');//.charAt(0);
  return `${anio}${mes}${dia}${hora}${minutos}${segundos}`;//14 digitos
};
/**
 * @abstract Método utilziado para utilizar la fecha como una ruta de KEYS utilizada para los Markers
 * @returns string de la fecha separa por diagonales
 */
export const DATE_getTime_for_key = () => {
  const fecha = new Date();
  const anio = fecha.getFullYear().toString();
  const mes = (fecha.getMonth() + 1).toString().padStart(2, '0');
  const dia = fecha.getDate().toString().padStart(2, '0');
  const hora = fecha.getHours().toString().padStart(2, '0');
  const minutos = fecha.getMinutes().toString().padStart(2, '0');
  return `${anio}/${mes}/${dia}/${hora}/${minutos}`;//12 digitos
};
/**
 * @abstract Método utilziado para aumentar minutos a la fecha para los Markers
 * @returns string de la fecha separa por diagonales
 */
export const DATE_getTime_added_minutes = (currentDateTime: string, minutesToAdd: number) => {
  const fecha = new Date(currentDateTime);
  fecha.setMinutes(fecha.getMinutes() + minutesToAdd);

  const anio = fecha.getFullYear().toString();
  const mes = (fecha.getMonth() + 1).toString().padStart(2, '0');
  const dia = fecha.getDate().toString().padStart(2, '0');
  const hora = fecha.getHours().toString().padStart(2, '0');
  const minutos = fecha.getMinutes().toString().padStart(2, '0');

  return `${anio}/${mes}/${dia}/${hora}/${minutos}`; // 12 dígitos
};
export const DATE_getTime_isMayor_that_now = (dateTime: string) => {
  return new Date(dateTime) > new Date();
};
export const DATE_getDayOfYear = () => {
  const fecha = new Date();
  return Math.floor((Number(fecha) - Number(new Date(fecha.getFullYear(), 0, 0))) / 86400000);
};
export const DATE_getWeekOfYear = () => {
  return Math.round(DATE_getDayOfYear() / 7);
};
export const isDate = (date: string): boolean => {
  if (isEmpty(date)) return false;
  if (date.length !== 14) return false;
  if (/[^0-9]/.test(date)) return false;
  return true;
};
export const isEmpty = (s: any) => {
  return s == 0 || s == "0" || s == "" || s == "null" || s == null || s == "undefined" || s == undefined || s == "NaN";
};
export const isFull = (s: any) => {
  return !isEmpty(s);
};
const MAP_STRINGS_ordenarMayorToMenor = (map: Map<string, string>) => {
  //const arregloOrdenado: [number, string][] = Array.from(this.historyPublications.entries()).sort((a, b) => b[0] - a[0]);
  const arregloOrdenado: [string, string][] = Array.from(map.entries()).sort((a, b) => b[0].localeCompare(a[0]));
  return new Map(arregloOrdenado);
};
const MAP_STRINGS_ordenarMenorToMayor = (map: Map<string, string>) => {
  //const arregloOrdenado: [number, string][] = Array.from(this.historyPublications.entries()).sort((a, b) => a[0] - b[0]);
  const arregloOrdenado: [string, string][] = Array.from(map.entries()).sort();
  return new Map(arregloOrdenado);
};

//callback name:string, type:string, data:string, file:any
const FILE_upload = (c: Function) => {
  const fileInputPhoto = document.createElement('input');
  fileInputPhoto.type = 'file';
  fileInputPhoto.addEventListener('change', async (event: any) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = async () => {
      //const data = reader.result as string;
      //const url = "https://ipfs.io/ipfs/" + cid + "?name=" + file.name.replaceAll(" ", "%20") + "&type=" + file.type.toString();
      c(file.name, file.type.toString(), reader.result as string, file);
    }
    reader.readAsDataURL(file);
  });
  fileInputPhoto.click();
};